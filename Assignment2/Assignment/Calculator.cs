﻿using System;
namespace Assignment2
{
    public class Calculator
    {
        int[] numbers;
        public Calculator(int[] numbers)
        {
            this.numbers = numbers;
        }
        public int Sum()
        {
            int result = 0;
            foreach (int num in numbers) { result += num; }
            return result;
        }
        public int subtraction(int sub)
        {
            //int result = numbers[0];
            //foreach (int num in numbers) { result -= num; }
            //return result + numbers[0];
            return sum() - sub;
        }
        public int Division(int divideby)
        {
            if (numbers.Length==0||divideby==0)
            {
                return 0;
            }
            else
            {
                return sum() / divideby;
            }
        }
        public int Multiplication(int multiplyby)
        {
            return sum() * multiplyby;
        }
    }
}
